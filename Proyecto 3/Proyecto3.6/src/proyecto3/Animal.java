/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 *
 * @author randy
 */
public class Animal {
    public enum tipoAnimal {
        PERRO,
        GATO,
        CONEJO
    }
    private Random rand;
    private tipoAnimal tipo;
    private int edad;
    private ArrayList<Integer> horarioComida;
    private ArrayList<Integer> horarioCaricia;
    private ArrayList<Integer> horarioMedicamento;
    private ArrayList<Integer> horarioLimpieza;
    
   public Animal(tipoAnimal tipo, int edad){
        rand= new Random();
        this.tipo = tipo;
        this.edad = edad;
        
        //Quemado antes de implementar el JSON
        horarioComida = new ArrayList<>();
        int cantidad=0;
        cantidad= rand.nextInt(3);
        for (int i=0; i<cantidad+1; i++){
            int hora=rand.nextInt(24);
            while(horarioComida.contains(hora)){
                hora=rand.nextInt(24);
            }
            horarioComida.add(hora);
        }
        
        cantidad= rand.nextInt(3);
        horarioCaricia = new ArrayList<>();
        for (int i=0; i<cantidad+1; i++){
            int hora=rand.nextInt(24);
            while(horarioCaricia.contains(hora)){
                hora=rand.nextInt(24);
            }
            horarioCaricia.add(hora);
        }
        
        cantidad= rand.nextInt(4);
        horarioMedicamento = new ArrayList<>();
        for (int i=0; i<cantidad; i++){
             int hora=rand.nextInt(24);
             while(horarioMedicamento.contains(hora)){
                 hora=rand.nextInt(24);
             }
             horarioMedicamento.add(hora);
        }
        
        cantidad= rand.nextInt(3);
        horarioLimpieza = new ArrayList<>();
        for (int i=0; i<cantidad+1; i++){
            int hora=rand.nextInt(24);
            while(horarioLimpieza.contains(hora)){
                hora=rand.nextInt(24);
            }
            horarioLimpieza.add(hora);
         }
    }
    
    
    public Animal(tipoAnimal tipo, int edad, ArrayList<Integer> HComida, ArrayList<Integer> HLimpieza, ArrayList<Integer> HCurar, ArrayList<Integer> HCaricia){
        this.tipo = tipo;
        this.edad = edad;
        
        //Quemado antes de implementar el JSON
        horarioComida = HComida;
        
        horarioCaricia = HCaricia;
        
        horarioMedicamento = HCurar;
        
        horarioLimpieza = HLimpieza;
    }
    
    public int getEdad(){
        return edad;
    }
    
    public String getTipo(){
        return tipo.toString();
    }
    
    public ArrayList<Integer> getHComida(){
        return horarioComida;
    }
    
    public ArrayList<Integer> getHCaricia(){
        return horarioCaricia;
    }
    
    public ArrayList<Integer> getHLimpieza(){
        return horarioLimpieza;
    }
    
    public ArrayList<Integer> getHMedicamento(){
        return horarioMedicamento;
    }
}   
