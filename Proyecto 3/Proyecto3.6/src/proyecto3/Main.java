/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


/**
 *
 * @author randy
 */
public class Main extends Application {
    
    static Stage mainStage;
    static Scene scMenu;
    static Scene scAgregarMascota;
    static int numM = 0;
    private double xOffset = 0; 
    private double yOffset = 0;
    
    @Override
    public void start(Stage stage) throws Exception {
        mainStage = new Stage();
        Parent S1 = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        mainStage.initStyle(StageStyle.TRANSPARENT);
        Parent S2 = FXMLLoader.load(getClass().getResource("AgregarMascota.fxml"));
        scMenu = new Scene(S1);
        
        scAgregarMascota = new Scene(S2);
        
        S1.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        S1.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
        
        mainStage.setScene(scMenu);
        mainStage.show();
        mainStage.setScene(scMenu);
        mainStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        launch(args);
        
    }
    
    public static void actnBtnAgregarMascota(){
        mainStage.setScene(scAgregarMascota);
        mainStage.show();
    }
    
    
    public static void actnMenu(){
        mainStage.setScene(scMenu);
        mainStage.show();
    }
}
    
