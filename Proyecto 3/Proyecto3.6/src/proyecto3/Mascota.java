/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;

import java.util.ArrayList;

/**
 *
 * @author randy
 */
public class Mascota extends Animal{
    private String nombre;
    private int indicador[];
    private String apariencia;
    private boolean[] banderas;
    
    public Mascota(String nombre, tipoAnimal animal, String apariencia){
        super(animal, 5);
        this.nombre = nombre;
        indicador = new int[4];
        indicador[0] = 5; //Felicidad
        indicador[1] = 5; //Salud
        indicador[2] = 0; //Suciedad
        indicador[3] = 0; //Hambre
        this.apariencia = apariencia;
        banderas = new boolean[4];
        banderas[0] = true;//Felicidad
        banderas[1] = true;//Salud
        banderas[2] = true;//Suciedad
        banderas[3] = true;//Hambre
    
    }
        public Mascota(String nombre, tipoAnimal animal, String apariencia, ArrayList<Integer> HComida, ArrayList<Integer> HLimpieza, ArrayList<Integer> HCurar, ArrayList<Integer> HCaricia){
        super(animal, 5, HComida, HLimpieza, HCurar, HCaricia);
        this.nombre = nombre;
        indicador = new int[4];
        indicador[0] = 5; //Felicidad
        indicador[1] = 5; //Salud
        indicador[2] = 0; //Suciedad
        indicador[3] = 0; //Hambre
        this.apariencia = apariencia;
        banderas = new boolean[4];
        banderas[0] = true;//Felicidad
        banderas[1] = true;//Salud
        banderas[2] = true;//Suciedad
        banderas[3] = true;//Hambre
    
    }
    
    public String getNombre(){
        return nombre;
    }
    public void setApariencia(String apariencia){
        this.apariencia = apariencia;
    }

    public void setIndicador(int ind, int valor) {
        this.indicador[ind] = valor;
    }
    
    public int[] getIndicadores(){
        return indicador;
    }

  
    public void actualizarFelicidad(String operacion){
        if (operacion.equals("RESTAR")){
            indicador[0]--;
        }
        else {
            indicador[0]++;
        }
        
        if (indicador[0] == -1){
            indicador[0] = 0;
        }
        
        if (indicador[0] == 6){
            indicador[0] = 5;
        }
        
    }
    
    public void actualizarSalud(String operacion){
        if (operacion.equals("RESTAR")){
            indicador[1]--;
        }
        else {
            indicador[1]++;
        }
        
        if (indicador[1] == -1){
            indicador[1] = 0;
        }
        
        if (indicador[1] == 6){
            indicador[1] = 5;
        }
    }
    
    public void actualizarSuciedad(String operacion){
        if (operacion.equals("RESTAR")){
            indicador[2]--;
        }
        else if (operacion.equals("SUMAR")){
            indicador[2]++;
        }
        else {
            indicador[2] = 0;
        }
        
        if (indicador[2] == -1){
            indicador[2] = 0;
        }
        
        if (indicador[2] == 6){
            indicador[2] = 5;
        }
    }
    
    public void actualizarHambre(String operacion){
        if (operacion.equals("RESTAR")){
            indicador[3]--;
        }
        else if (operacion.equals("SUMAR")){
            indicador[3]++;
        }
        else {
            indicador[3] = 0;
        }
        
        if (indicador[3] == -1){
            indicador[3] = 0;
        }
        
        if (indicador[3] == 6){
            indicador[3] = 5;
        }
    }
    
    public void actualizarBanderas(int bandera, boolean valor){
        banderas[bandera] = valor;
    }
    
    public int getEdad() {
        return super.getEdad();
    }
    
    public boolean[] getBanderas(){
        return banderas;
    }
    
    @Override
    public String getTipo(){
        return super.getTipo();
    }
    
    @Override
    public ArrayList<Integer> getHComida(){
        return super.getHComida();
    }
    
    @Override
    public ArrayList<Integer> getHCaricia(){
        return super.getHCaricia();
    }
    
    @Override
    public ArrayList<Integer> getHLimpieza(){
        return super.getHLimpieza();
    }
    
    @Override
    public ArrayList<Integer> getHMedicamento(){
        return super.getHMedicamento();
    }
    
    public String getApariencia(){
        return apariencia;
    }
}
