/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author randy
 */
public class AgregarMascotaController implements Initializable {
    
    @FXML 
    JFXTextField txtFNombre;
    @FXML 
    JFXButton btnAtras;
    @FXML
    JFXComboBox cbAnimal;
    @FXML
    JFXButton btnAgregar;
    @FXML
    ImageView img;
    @FXML
    JFXButton btnIzquierda;
    @FXML
    JFXButton btnDerecha;
    
    String tipo="";
    
    ObservableList<String> animales;
    
    private int contador = 0;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        animales = FXCollections.observableArrayList("Gato", "Perro", "Conejo");
        cbAnimal.setItems(animales);
    }
    
    @FXML
    public void actnBtnAgregar(ActionEvent event){
        switch (tipo) {
            case "Gato":
                FXMLDocumentController.jugador.agregarMascota(txtFNombre.getText(), Animal.tipoAnimal.GATO, "/IMG/gatos ("+ contador +").jpg");
                break;
            case "Perro":
                FXMLDocumentController.jugador.agregarMascota(txtFNombre.getText(), Animal.tipoAnimal.GATO, "/IMG/perros ("+ contador +").jpeg");
                break;
            case "Conejo":
                FXMLDocumentController.jugador.agregarMascota(txtFNombre.getText(), Animal.tipoAnimal.GATO, "/IMG/conejos ("+ contador +").jpg");
                break;
        }
        Main.actnMenu();
        txtFNombre.setText("");
        cbAnimal.setValue("");
        img.setImage(null);
        tipo="";
    }
    
    public void setImagen(String tipo){
        if (tipo.equals("Perro")){
            Image x = new Image("/IMG/perros (0).jpeg");
            img.setImage(x);
        }else if (tipo.equals("Gato")){
            Image x = new Image("/IMG/gatos (0).jpg");
            img.setImage(x);
        }else if(tipo.equals("Conejo")){
            Image x = new Image("/IMG/conejos (0).jpg");
            img.setImage(x);
        }
    }
    
    @FXML
    public void actnBtnAtras(ActionEvent event){
        Main.actnMenu();
    }
    
    @FXML
    public void actBtnIzquierda(){
        if (contador == 0){
            contador = 5;
        }
        else {
            contador --;
        }
        
        if (tipo.equals("Perro")){
            Image x = new Image("/IMG/perros ("+ contador+").jpeg");
            img.setImage(x);
        }else if (tipo.equals("Gato")){
            Image x = new Image("/IMG/gatos ("+ contador+").jpg");
            img.setImage(x);
        }else if(tipo.equals("Conejo")){
            Image x = new Image("/IMG/conejos ("+ contador+").jpg");
            img.setImage(x);
        }
        
    }
    
    @FXML
    void seleccionarTipo(ActionEvent event) {
        tipo= (String) cbAnimal.getSelectionModel().getSelectedItem();
        this.setImagen(tipo);
    }
    
    @FXML
    public void actBtnDerecha(){
        if (contador == 5){
            contador = 0;
        }
        else {
            contador ++;
        }

        if (tipo.equals("Perro")){
            Image x = new Image("/IMG/perros ("+ contador+").jpeg");
            img.setImage(x);
        }else if (tipo.equals("Gato")){
            Image x = new Image("/IMG/gatos ("+ contador+").jpg");
            img.setImage(x);
        }else if(tipo.equals("Conejo")){
            Image x = new Image("/IMG/conejos ("+ contador+").jpg");
            img.setImage(x);
        }
        
    }
    
    
}
