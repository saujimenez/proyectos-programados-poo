/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;
import java.io.FileReader;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Kevin Zumbado
 */
public class LeeJSON {
    
    private Jugador owner;
    
    public LeeJSON(){
        FileReader fread;
        String filename="src/js/datos.json";
        
        try {
            JsonParser parser = new JsonParser() ;
            fread = new FileReader(new File(filename));
            
            JsonElement datos = parser.parse(fread);
            JsonObject obj = datos.getAsJsonObject();
            
            java.util.Set<java.util.Map.Entry<String,JsonElement>> entradas = obj.entrySet();
            java.util.Iterator<java.util.Map.Entry<String,JsonElement>> iter = entradas.iterator();
            java.util.Map.Entry<String,JsonElement> entrada = iter.next();
            JsonArray gsonArr = entrada.getValue().getAsJsonArray();
            
            for (JsonElement objArr : gsonArr) {
                JsonObject gsonObj = objArr.getAsJsonObject();
                String jugador = gsonObj.get("usuario").getAsString();
                 owner = new Jugador(jugador);
                
                JsonArray mascota  = gsonObj.get("mascota").getAsJsonArray();
                String listo="";
                for (JsonElement objArr2 : mascota) {

                    JsonObject gsonObj2 = objArr2.getAsJsonObject();

                    String name = gsonObj2.get("nombre").getAsString();
                    if(!listo.equals(name)){
                        String tipo = gsonObj2.get("tipo").getAsString();
                        String apariencia = gsonObj2.get("apariencia").getAsString();
                        JsonArray horarioCo = gsonObj2.get("horarioComida").getAsJsonArray();
                        JsonArray horarioLi = gsonObj2.get("horarioLimpiar").getAsJsonArray();
                        JsonArray horarioCu = gsonObj2.get("horarioMedicar").getAsJsonArray();
                        JsonArray horarioAca = gsonObj2.get("horarioCaricia").getAsJsonArray();

                        ArrayList<Integer> horarioComida = new ArrayList();
                        ArrayList<Integer> horarioLimpieza = new ArrayList();
                        ArrayList<Integer> horarioCurar = new ArrayList();
                        ArrayList<Integer> horarioAcariciar = new ArrayList();

                        for (int i=0; i<horarioCo.size();i++){
                            horarioComida.add(horarioCo.get(i).getAsInt());
                        }
                        for (int i=0; i<horarioLi.size();i++){
                            horarioLimpieza.add(horarioLi.get(i).getAsInt());
                        }
                        for (int i=0; i<horarioCu.size();i++){
                            horarioCurar.add(horarioCu.get(i).getAsInt());
                        }
                        for (int i=0; i<horarioAca.size();i++){
                            horarioAcariciar.add(horarioAca.get(i).getAsInt());
                        }
                        if(tipo.equals("Perro")){  
                            owner.agregarMascota(name, Animal.tipoAnimal.PERRO, apariencia,horarioComida,horarioLimpieza,horarioCurar,horarioAcariciar);
                        }
                        if(tipo.equals("Gato")){  
                            owner.agregarMascota(name, Animal.tipoAnimal.GATO, apariencia,horarioComida,horarioLimpieza,horarioCurar,horarioAcariciar);
                        }
                        if(tipo.equals("Conejo")){  
                            owner.agregarMascota(name, Animal.tipoAnimal.CONEJO, apariencia,horarioComida,horarioLimpieza,horarioCurar,horarioAcariciar);
                        }
                    }

                    listo=name;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LeeJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Jugador getJugador(){
        return owner;
    }
}
