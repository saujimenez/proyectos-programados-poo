/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;
import com.jfoenix.controls.JFXComboBox;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;


public class FXMLDocumentController implements Initializable{
   
    
    
    static LeeJSON lee = new LeeJSON();
    static Jugador jugador = lee.getJugador();
    
    ArrayList<String> hilos = new ArrayList();
    
    Controlador controlador = new Controlador(jugador);
    
    String nombre = "";
   
    @FXML
    private Label lblJugador;
    
    @FXML
    private ImageView ivSalud;
    
    @FXML
    private ImageView ivHigiene;
    
    @FXML
    private ImageView ivHambre;
    
    @FXML
    private ImageView ivFelicidad;
    
    private ObjectProperty<Image> imageProperty = new SimpleObjectProperty<>();
    @FXML
    private ImageView img;
    
    @FXML
    private ImageView ivHora;
    
    @FXML
    private ImageView ivTiempo;
    
    @FXML
    private /*static*/ Label lblPuntaje;
    
    @FXML
    private Label lblDia;
    
   
    private ObservableList<String> mascotas = FXCollections.observableArrayList();
    
    @FXML
    private JFXComboBox cbMascotas;
    
    @FXML
    void actnBtnAgregarMascota(ActionEvent event) {
        Main.actnBtnAgregarMascota();
    }
    
    public void nuevaMascota(String nombre, Animal.tipoAnimal tipo, String apariencia){
        jugador.agregarMascota(nombre, tipo, apariencia);
    }
    
    @FXML
    void cargarMascota(ActionEvent event) {
        try{
            if (cbMascotas.getItems().size()>0){
                nombre=mascotas.get(cbMascotas.getSelectionModel().getSelectedIndex());
                controlador.setMascoActual(controlador.desplegarMascota(nombre));
                img.imageProperty().bind(controlador.getTumba());
                if (!hilos.contains(controlador.getMascoActual().getNombre())){
                    new Thread(controlador).start();
                    hilos.add(controlador.getMascoActual().getNombre());
                }
                controlador.desplegarIndicadoresActualizados();
            }
        }catch(Exception e){
            //System.out.println(e+"error");
        }
        
    }
   
    @FXML
    void seleccionarMascota(MouseEvent event) {
        
        try{
            int largo=mascotas.size();

            for(int i=0; i<jugador.getMascotas().size(); i++){
                if(!mascotas.contains(jugador.getMascotas().get(i).getNombre())){
                    mascotas.add(jugador.getMascotas().get(i).getNombre());
                }
            }
            
            if(mascotas.size()!=largo){
                
                String seleccion= (String) cbMascotas.getSelectionModel().getSelectedItem();
                
                this.cbMascotas.setItems(mascotas);

                cbMascotas.setValue(seleccion);
            }
        }catch(Exception e){
             System.out.println(e);
        }
    }
    


    @FXML
    void actnBtnSalir(ActionEvent event) {
        System.exit(0);
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
//To change body of generated methods, choose Tools | Templates.
        for(int i=0; i<jugador.getMascotas().size(); i++){
            if(!mascotas.contains(jugador.getMascotas().get(i).getNombre())){
                mascotas.add(jugador.getMascotas().get(i).getNombre());
            }
        }
        this.cbMascotas.setItems(mascotas);
        
        lblPuntaje.textProperty().bind(controlador.titleProperty());
        ivHora.imageProperty().bind(controlador.reloj);
        ivTiempo.imageProperty().bind(controlador.clima);
        lblDia.textProperty().bind(controlador.messageProperty());
        lblJugador.setText(jugador.getNombre());
        
        ivSalud.imageProperty().bind(controlador.indSalud);
        ivHigiene.imageProperty().bind(controlador.indHigiene);
        ivHambre.imageProperty().bind(controlador.indHambre);
        ivFelicidad.imageProperty().bind(controlador.indFelicidad);

    }
    
    public void actBtnAcariciar(){
        controlador.actualizarNecesidades(0, controlador.getMascoActual());
    }
    
    public void actBtnCurar(){
        controlador.actualizarNecesidades(1, controlador.getMascoActual());
    }
    
    public void actBtnLimpiar() {
        controlador.actualizarNecesidades(2, controlador.getMascoActual());
    }
    
    public void actBtnAlimentar(){
        controlador.actualizarNecesidades(3, controlador.getMascoActual());
    }
}
