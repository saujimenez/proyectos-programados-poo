/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;

import java.util.ArrayList;

/**
 *
 * @author randy
 */
public class Jugador {
    private ArrayList<Mascota> mascotas;
    private String nombre;
    private int puntaje;
    
    public Jugador(String nombre){
        this.nombre = nombre;
        this.puntaje = 0;
        mascotas = new ArrayList();
    }

    public String getNombre() {
        return nombre;
    }
    
    public void agregarMascota(String nombre, Animal.tipoAnimal animal, String apariencia){
        mascotas.add(new Mascota(nombre,animal, apariencia));
    }
    
    public void agregarMascota(Mascota mascota){
        mascotas.add(mascota);
        
    }
    
    public void agregarMascota(String nombre, Animal.tipoAnimal animal, String apariencia, ArrayList<Integer> HComida, ArrayList<Integer> HLimpieza, ArrayList<Integer> HCurar, ArrayList<Integer> HCaricia){
        mascotas.add(new Mascota(nombre,animal, apariencia,HComida,HLimpieza,HCurar,HCaricia));
    }
    
    public void eliminarMascota(Mascota mascota){
        mascotas.remove(mascota);
    }
    
    public Mascota seleccionarMascota(String nombre){
        
        for (int i = 0; i<mascotas.size(); i++){
            if (mascotas.get(i).getNombre().equals(nombre)){
                return mascotas.get(i);
            }
        }
        return mascotas.get(0);//para que Java no joda, nunca va a llegar hasta aquí
    }

    public ArrayList<Mascota> getMascotas() {
        return mascotas;
    }
    
    
    
    public void imprimirMascotas(){
        for (int i = 0; i < mascotas.size(); i ++){
            System.out.println("Mascota " + (i+1));
            System.out.println("Nombre: " + mascotas.get(i).getNombre());
            System.out.println("Edad: " + mascotas.get(i).getEdad());
            System.out.println("Animal: " + mascotas.get(i).getTipo());
            System.out.println("----------------------------------");
        }
    }
    
    public int getPuntaje(){
        return puntaje;
    }
    
    public void setPuntaje(int puntaje){
        this.puntaje = puntaje;
    }
}
