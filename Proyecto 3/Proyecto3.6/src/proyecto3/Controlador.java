/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.scene.image.Image;

/**
 *
 * @author randy
 */
public class Controlador extends Task{
    private Jugador jugador;
    private int tiempo;
    public ObjectProperty<Image> reloj = new SimpleObjectProperty<>();
    public ObjectProperty<Image> clima = new SimpleObjectProperty<>();
    public ObjectProperty<Image> imgTumba = new SimpleObjectProperty<>();
    public ObjectProperty<Image> indSalud = new SimpleObjectProperty<>();
    public ObjectProperty<Image> indHigiene = new SimpleObjectProperty<>();
    public ObjectProperty<Image> indHambre = new SimpleObjectProperty<>();
    public ObjectProperty<Image> indFelicidad = new SimpleObjectProperty<>();
    
    private Mascota mascoActual;
    private int horasRestantes = 2;
    private String[] diasSemana;
    public int diaActual;
    
    public Controlador(Jugador jugador){
        this.jugador = jugador;
        tiempo = 6;
        diasSemana = new String[7];
        diasSemana[0] = "Lun.";
        diasSemana[1] = "Mar.";
        diasSemana[2] = "Mie.";
        diasSemana[3] = "Jue.";
        diasSemana[4] = "Vie.";
        diasSemana[5] = "Sab.";
        diasSemana[6] = "Dom.";
        diaActual = 0;
        mascoActual=null;
        
    }
    
    public Mascota desplegarMascota(String nombre){
        return jugador.seleccionarMascota(nombre);
    }

    public Mascota getMascoActual() {
        return mascoActual;
    }
    
    public void setMascoActual(Mascota mascota){
        this.mascoActual=mascota;
    }
    
    public void actualizarTiempo(){
        if (tiempo == 23){
            tiempo = 0;
            if (diaActual == 6){
                diaActual = 0;
            }
            else {
                diaActual ++;
            }
        }
        else {
            tiempo ++;
        }
    }
    
    public void actualizarPuntaje(){
        int puntaje = jugador.getPuntaje();
        ArrayList<Mascota> mascotas = jugador.getMascotas();
        

        
        for (int i = 0; i<mascotas.size(); i++){
            
            if (mascotas.get(i).getApariencia().equals("/IMG/tumba.png")){
                continue;
            }
            
            int felicidad = mascotas.get(i).getIndicadores()[0];
            int salud = mascotas.get(i).getIndicadores()[1];
            int suciedad = mascotas.get(i).getIndicadores()[2];
            int hambre = mascotas.get(i).getIndicadores()[3];
            
            
            if (felicidad > 3){//Si la felicidad es mayor a 3 se le suman 10 pts
                puntaje += 10;
            }else {
                puntaje -= 10;
            }
            
            if (salud == 5){//Si la salud es 5 se suma 10
                puntaje += 10;
            }else if (salud <= 2){//Si la salud esta entre 0 y 2
                puntaje -= 10;
            }
            
            if (suciedad >= 4){
                puntaje -= 10;
            }
            
            if (hambre < 4){
                puntaje -= 10;
            }
        }
        jugador.setPuntaje(puntaje);
    }
 
    
    public void actualizarIndicadores(){
        ArrayList<Mascota> mascotas = jugador.getMascotas();
        for (int i = 0; i<mascotas.size(); i++){
            
            if (mascotas.get(i).getApariencia().equals("/IMG/tumba.png")){
                continue;
            }
            
            if (mascotas.get(i).getBanderas()[3] == false){//Si la mascota no fue alimentada se le baja la salud y el hambre
                mascotas.get(i).actualizarHambre("SUMAR");
                mascotas.get(i).actualizarSalud("RESTAR");
            }
            if (mascotas.get(i).getBanderas()[2] == false){//Si no fue limpiada, se le suma la suciedad y le baja la salud
                mascotas.get(i).actualizarSuciedad("SUMAR");
                mascotas.get(i).actualizarSalud("RESTAR");
            }
            if (mascotas.get(i).getBanderas()[0] == false){//Si no fue acariciada, le baja la felicidad
                mascotas.get(i).actualizarFelicidad("RESTAR");
            }
            if (mascotas.get(i).getBanderas()[1] == false){//Si no se le dieron los medicamentos, se le baja la salud
                mascotas.get(i).actualizarSalud("RESTAR");
            }
                
            
        }
    }
    
    public void verificarHorarios(){
        ArrayList<Mascota> mascotas = jugador.getMascotas();
        
        for (int i = 0; i<mascotas.size(); i++){
            
            if (mascotas.get(i).getApariencia().equals("/IMG/tumba.png")){
                continue;
            }
            
            Iterator caricias = mascotas.get(i).getHCaricia().iterator();
            Iterator medicamentos = mascotas.get(i).getHMedicamento().iterator();
            Iterator limpiezas = mascotas.get(i).getHLimpieza().iterator();
            Iterator comidas = mascotas.get(i).getHComida().iterator();
            
            //Verifica horarios de caricias
            while (caricias.hasNext()){
                if (tiempo == (int) caricias.next()){
                    mascotas.get(i).actualizarFelicidad("RESTAR");
                    mascotas.get(i).actualizarBanderas(0, false);
                }
            }
            
            //Verifica horarios de medicamentos
            while (medicamentos.hasNext()){
                if (tiempo == (int) medicamentos.next()){
                    mascotas.get(i).actualizarSalud("RESTAR");
                    mascotas.get(i).actualizarBanderas(1, false);
                }
            }
            
            //Verifica horarios de limpiezas
            while (limpiezas.hasNext()){
                if (tiempo == (int) limpiezas.next()){
                    mascotas.get(i).actualizarSuciedad("SUMAR");
                    mascotas.get(i).actualizarFelicidad("RESTAR");
                    mascotas.get(i).actualizarBanderas(2, false);
                }
            }
            
            //Verifica horarios de comidas
            while (comidas.hasNext()){
                if (tiempo == (int) comidas.next()){
                    mascotas.get(i).actualizarHambre("SUMAR");
                    mascotas.get(i).actualizarSalud("RESTAR");
                    mascotas.get(i).actualizarBanderas(3, false);
                }
            }
            
            
        }
    }
    
    public float getHora(){
        return tiempo;
    }
    
    public String getDia(){
        return diasSemana[diaActual];
    }
    
    public void desplegarIndicadoresActualizados(){
        indFelicidad.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[0] + ").jpg"));
        indSalud.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[1] + ").jpg"));
        indHigiene.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[2] + ").jpg"));
        indHambre.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[3] + ").jpg"));
    }
    
    public ObjectProperty getTumba(){
        imgTumba.set(new Image(mascoActual.getApariencia()));
        return imgTumba;
    }
    
    @Override
    protected Integer call() throws Exception{
        int tiempo12H;
        updateMessage(getDia());
        
        while (true){
            imgTumba.set(new Image(mascoActual.getApariencia()));
                try {
                    Thread.sleep(5000);
                    
                    actualizarTiempo();
                    tiempo12H = tiempo;
                        if (tiempo12H >= 12){
                            tiempo12H -= 12;
                        }
                        if (tiempo == 0){
                            updateMessage(getDia());
                        }

                    reloj.set(new Image("/IMG/hora (" + String.valueOf(tiempo12H) + ").jpg"));
                    
                    actualizarIndicadores();
                    verificarHorarios();
                    actualizarPuntaje();
                        

                    indFelicidad.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[0] + ").jpg"));
                    indSalud.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[1] + ").jpg"));
                    indHigiene.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[2] + ").jpg"));
                    indHambre.set(new Image("/IMG/indicadores (" + desplegarMascota(mascoActual.getNombre()).getIndicadores()[3] + ").jpg"));

                    if (tiempo == 5){
                        clima.set(new Image("/IMG/tiempo (0).jpg"));
                    }
                    else if(tiempo == 7){
                        clima.set(new Image("/IMG/tiempo (1).jpg"));
                    }
                    else if(tiempo == 12){
                        clima.set(new Image("/IMG/tiempo (2).jpg"));
                    }
                    else if(tiempo == 17){
                        clima.set(new Image("/IMG/tiempo (5).jpg"));
                    }
                    else if(tiempo == 19){
                        clima.set(new Image("/IMG/tiempo (3).jpg"));
                    }


                    updateTitle(String.valueOf(jugador.getPuntaje()));

                        
                    /*
                    System.out.println("Felicidad = " + mascoActual.getIndicadores()[0]);
                    System.out.println("Salud = " + mascoActual.getIndicadores()[1]);
                    System.out.println("Suciedad = " + mascoActual.getIndicadores()[2]);
                    System.out.println("Hambre = " + mascoActual.getIndicadores()[3]);
                    System.out.println("Día: " + getDia() + "  Hora: " + getHora());
                    System.out.println("Puntaje actual: " + jugador.getPuntaje());
                    System.out.println("\n\n");
                    */
                    
                } catch (InterruptedException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            comprobarIndicadores();
            imgTumba.set(new Image(mascoActual.getApariencia()));
        }
    }
    
    public void actualizarNecesidades(int necesidad, Mascota mascota){
        try{
            if (mascota.getApariencia().equals("/IMG/tumba.png")){
            return;
            }

            if (necesidad == 0){//Felicidad
                mascota.actualizarFelicidad("SUMAR");
                mascota.actualizarBanderas(0, true);
                indFelicidad.set(new Image("/IMG/indicadores ("+ desplegarMascota(mascota.getNombre()).getIndicadores()[0] +").jpg"));
            }
            else if (necesidad == 1){//Salud
                mascota.actualizarSalud("SUMAR");
                mascota.actualizarBanderas(1, true);
                indSalud.set(new Image("/IMG/indicadores ("+ desplegarMascota(mascota.getNombre()).getIndicadores()[1] +").jpg"));
            }
            else if (necesidad == 2){//Limpieza
                mascota.actualizarSuciedad("A CERO");
                mascota.actualizarBanderas(2, true);
                indHigiene.set(new Image("/IMG/indicadores ("+ desplegarMascota(mascota.getNombre()).getIndicadores()[2] +").jpg"));
            }
            else {//Comida
                mascota.actualizarHambre("A CERO");
                mascota.actualizarSalud("SUMAR");
                mascota.actualizarBanderas(3, true);
                indHambre.set(new Image("/IMG/indicadores ("+ desplegarMascota(mascota.getNombre()).getIndicadores()[3] +").jpg"));
            }
        }catch(Exception e){
        
        }

    }
    public void comprobarIndicadores(){
        ArrayList<Mascota> mascotas = jugador.getMascotas();
        
        for (int i = 0; i<mascotas.size(); i++){
            
            if (mascotas.get(i).getApariencia().equals("/IMG/tumba.png")){
                continue;
            }
            
            if (mascotas.get(i).getIndicadores()[1] == 0){
                
                if (mascotas.get(i).getIndicadores()[3] == 5){
                    mascotas.get(i).setApariencia("/IMG/tumba.png");
                }
                
                if (horasRestantes == 0){
                    mascotas.get(i).setApariencia("/IMG/tumba.png");
                }
                else {
                    horasRestantes--;
                }
            }
        }
    }

}