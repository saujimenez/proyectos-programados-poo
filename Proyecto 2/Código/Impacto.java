/*
Progragación Orientada a Objetos. Proyecto #2 Batalla Naval
Instituto Tecnológico de Costa Rica. II Semestre 2019
Prof. Samanta Ramijan Carmiol

Estudiantes:
          Randy Conejo Juárez
          Kevin Zumbado Cruz
          Saúl Jiménez González
*/

public interface Impacto {
  //Metodos
  public abstract void reducirVida();
}