/*
Progragación Orientada a Objetos. Proyecto #2 Batalla Naval
Instituto Tecnológico de Costa Rica. II Semestre 2019
Prof. Samanta Ramijan Carmiol

Estudiantes:
          Randy Conejo Juárez
          Kevin Zumbado Cruz
          Saúl Jiménez González
*/

import java.util.ArrayList;
public class Jugador {
  //Atributos
  private ArrayList<Integer> movimiento; 
  private ArrayList<Integer> acierto;
  private ArrayList<Barco> flota;

  //Constructor
  public Jugador(){
    flota = new ArrayList<>();
    crearFlota();
    movimiento = new ArrayList<>();
    acierto = new ArrayList<>();
  }

  //Getters y Setters
  public ArrayList<Integer> getAcierto(){
    return this.acierto;
  }

  public ArrayList<Integer> getMovimiento(){
    return this.movimiento;
  }

  //Métodos

  public void crearFlota(){
    //Crea la flota para cada jugador
    flota.add(new Barco("Coraza", 1));
    flota.add(new Barco("Coraza", 1));
    flota.add(new Barco("Destructor", 2));
    flota.add(new Barco("Destructor", 2));
    flota.add(new Barco("Destructor", 2));
    flota.add(new Barco("Submarino", 3));
    flota.add(new Barco("Submarino", 3));
    flota.add(new Barco("Submarino", 3));
    flota.add(new Barco("Portaaviones", 4));
  }

  public ArrayList<Barco> getFlota() {
    return this.flota;
  }
}