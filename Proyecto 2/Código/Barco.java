/*
Progragación Orientada a Objetos. Proyecto #2 Batalla Naval
Instituto Tecnológico de Costa Rica. II Semestre 2019
Prof. Samanta Ramijan Carmiol

Estudiantes:
          Randy Conejo Juárez
          Kevin Zumbado Cruz
          Saúl Jiménez González
*/

import java.util.ArrayList;

public class Barco implements Impacto{
  //Atributos
  private String nombre;
  private int casillas;
  private int vida;
  private ArrayList<String> casillasOcupadas;

  //Constructor
  public Barco(String nombre, int casillas){
    this.nombre = nombre;
    this.casillasOcupadas = new ArrayList<>();
    this.casillas = casillas;
    this.vida = casillas;
  }
  
  //Getters y Setters
  public String getNombre(){
    return this.nombre;
  }
  public void setNombre(String nombre){
    this.nombre = nombre;
  }

  public int getCasillas(){
    return this.casillas;
  }
  public void setCasillas(int casillas){
    this.casillas = casillas;
  }
  
  public int getVida(){
      return this.vida;
  }

  @Override
  public void reducirVida(){
      this.vida --;
  }
  
}


