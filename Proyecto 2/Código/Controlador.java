/*
Progragación Orientada a Objetos. Proyecto #2 Batalla Naval
Instituto Tecnológico de Costa Rica. II Semestre 2019
Prof. Samanta Ramijan Carmiol

Estudiantes:
          Randy Conejo Juárez
          Kevin Zumbado Cruz
          Saúl Jiménez González
*/

public class Controlador {
  //Atributos
  private Tablero tableroJugador1;
  private Tablero tableroJugador2;

  private Jugador jugador1;
  private Jugador jugador2;

  //Constructor
  public Controlador(){
    
    jugador1 = new Jugador();
    jugador2 = new Jugador();
    
    tableroJugador1= new Tablero(jugador1);
    tableroJugador2= new Tablero(jugador2);

    solicitarPosiciones();

    inicio();
    
    determinarGanador();

  }

  public void determinarGanador(){
    //Esta función es llamada cuando uno de los dos jugadores se quedó sin barcos, entonces se determina cual fue el ganador
    if (jugador1.getFlota().size()==0){
      System.out.println("\n\nGANADOR: Jugador 2\n\n");
    }else{
      System.out.println("\n\nGANADOR: Jugador 1\n\n");
    }
  }

  public void inicio(){
    //Transcurso del juego, aqui se intercambia entre tableros de ambos jugadores
    while(jugador1.getFlota().size()!=0 && jugador2.getFlota().size()!=0){

      System.out.print("\n\n\n\n\n\n\n\n\nJUGADOR 1 ES TU TURNO");
      try {
            Thread.sleep(1*1000);
      } catch (Exception e) {
            System.out.println(e);
      }

      
      System.out.println("\n\n");
      System.out.println("Tu Flota\n");
      tableroJugador1.imprimirTablero();
      System.out.println("\n\nTablero Rival");
      tableroJugador1.imprimirTableroMovimiento();
      tableroJugador1.registrarMovimiento(tableroJugador2.getPosicionamiento(),jugador2);
      if (jugador2.getFlota().size() == 0){
        break;
      }


      System.out.println("\n\n\n\n\n\n\n\n\n");

      


      System.out.print("\nJUGADOR 2 ES TU TURNO");
      try {
            Thread.sleep(1*1000);
      } catch (Exception e) {
            System.out.println(e);
      }

      
      System.out.println("\n\n");
      System.out.println("Tu Flota\n");
      tableroJugador2.imprimirTablero();
      System.out.println("\n\nTablero Rival");
      tableroJugador2.imprimirTableroMovimiento();
      tableroJugador2.registrarMovimiento(tableroJugador1.getPosicionamiento(),jugador1);

    }
  }

  public void solicitarPosiciones(){
    //Llama a las funciones donde se pide al usuario colocar barcos, no recibe ni retorna nada
    System.out.println("\nJugador 1");
    tableroJugador1.imprimirTablero();
    tableroJugador1.posicionarBarcos();

    System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");



    System.out.println("\nJugador 2");
    tableroJugador2.imprimirTablero();
    tableroJugador2.posicionarBarcos();
  }

}