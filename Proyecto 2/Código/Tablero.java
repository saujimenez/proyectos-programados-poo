/*
Progragación Orientada a Objetos. Proyecto #2 Batalla Naval
Instituto Tecnológico de Costa Rica. II Semestre 2019
Prof. Samanta Ramijan Carmiol

Estudiantes:
          Randy Conejo Juárez
          Kevin Zumbado Cruz
          Saúl Jiménez González
*/


import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class Tablero {
  //Atributos
  private Jugador jugador;
  private Barco[][] posicionamiento;
  Random rand = new Random();

  Scanner entrada = new Scanner(System.in);
  //-----------------------------------------------------------------------------------------------------------------

  //Constructor
  public Tablero(Jugador jugador){
    this.jugador = jugador;
    this.posicionamiento = new Barco[10][10];
    for (int k=0; k<10; k++){
      for(int i=0; i<10; i++){
        posicionamiento[k][i] = new Barco("NULL", -1);
      }
    }
  }

  //-----------------------------------------------------------------------------------------------------------------

  //Getters y Setters
  public Jugador getJugador(){
    return this.jugador;
  }
  public void setJugador(Jugador jugador){
    this.jugador = jugador;
  }

  public Barco[][] getPosicionamiento(){
    return this.posicionamiento;
  }
  public void setPosicionamiento(Barco[][] posicionamiento){
    this.posicionamiento = posicionamiento;
  }

  //-----------------------------------------------------------------------------------------------------------------
  public void imprimirTableroMovimiento(){
    //Método que simula un tablero mediante el uso de caracteres ascii, además pinta los disparos realizados por el usuario y dependiendo de si le dió a un barco o no se pintan, no recibe ni retorna nada
    System.out.println("\n");
    String[] list2= {"A","B","C","D","E","F","G","H","I","J"};
    System.out.print("    1   2   3   4   5   6   7   8   9  10 \n");
    for (int k=0; k<10; k++){
      
      System.out.print( "  \u001B[46;37m.---------------------------------------.\u001B[0m\n");
      
      System.out.print(list2[k]+" " + "\u001B[46;37m");
      
      for(int i=0; i<10; i++){
        
        int posicion = k*10 + i;
        
        if(jugador.getMovimiento().contains(posicion)){

          System.out.print("│ X ");
        
        }
        else if(jugador.getAcierto().contains(posicion)){
          System.out.print("│ \u001B[46;31mX\u001B[46;37m ");
        }
        else{
          System.out.print("│   ");
        
        }
          
      }
      System.out.print("│" + "\u001B[0m" + "\n");
    }
    System.out.print( "  \u001B[46;37m·---------------------------------------·\u001B[0m\n");
  }
  

  public void imprimirTablero(){
    //Método que simula un tablero mediante el uso de caracteres ascii, además ayuda al jugador a ubicar sus barcos y poder verlos durante toda la partida, no recibe ni retorna nada

    
    System.out.print("\n");
    
    String[] list2= {"A","B","C","D","E","F","G","H","I","J"};
    
    System.out.print("    1   2   3   4   5   6   7   8   9  10 \n");
    
    boolean flag=false;
    boolean impreso=false;
    
    ArrayList<Integer> valorI = new ArrayList<Integer>();
    
    int cont=0;
    for (int k=0; k<10; k++){

          for(int i=0; i<10; i++){
                
                if(impreso==false){
                  impreso=true;
                  if (flag){
                      flag=false;
                      int[] valores={2, 6, 10, 14, 18, 22, 26, 30, 34, 38};
                      System.out.print("  \u001B[46;37m");
                      for (int j=0; j<41; j++){
                        if (j == 0 || j == 40){
                        System.out.print(".");
                        }else if(j+1==valores[valorI.get(cont)]){
                          System.out.print("\u001B[46;30m █ \u001B[46;37m");
                          j+=2;
                          if (valorI.size()-1>cont){
                            cont++;
                          }
                        }else{
                          System.out.print("-");
                        }
                      }
                      valorI.removeAll(valorI);
                      cont=0;
                      System.out.print("\u001B[0m\n");
                      System.out.print(list2[k]+" ");
                  }else{
                    System.out.print("  \u001B[46;37m.---------------------------------------.\u001B[0m\n");
                    System.out.print(list2[k]+" ");
                  }
                }

          if (posicionamiento[k][i].getCasillas() == -1){
            
              int agua= rand.nextInt(2);
              if (agua==0){
                System.out.print("\u001B[46;37m│ _.\u001B[0m");
              }else{
                System.out.print("\u001B[46;37m│ ~.\u001B[0m"); 
              }

          }else if (posicionamiento[k][i].getCasillas() == 0 ){
            
            System.out.print("\u001B[46;37m│\u001B[46;30m X ");


            if(i!=9){
                if(posicionamiento[k][i+1].getCasillas()==posicionamiento[k][i+1].getVida() && !posicionamiento[k][i+1].getNombre().equals("DEL")){
                  continue;
                }

                if(posicionamiento[k][i+1].getCasillas()==2 && posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida()){
                  System.out.print("■■ >\u001B[0m");
                  i++;
                }else if(posicionamiento[k][i+1].getCasillas()==3 || posicionamiento[k][i+1].getNombre().equals("DEL")){
                  
                    if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                      if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                        System.out.print("■■■■");
                        i++;
                      
                      if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                        System.out.print("■ > \u001B[0m");
                        i++;
                      }else{
                        System.out.print(" X  \u001B[0m");
                        i++;
                      }
                    }else{
                        System.out.print(" X  \u001B[0m");
                        i++;
                      if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                        System.out.print("■ > \u001B[0m");
                        i++;
                      }else{
                        System.out.print(" X  \u001B[0m");
                        i++;
                      }
                    }

                }else if(posicionamiento[k][i+1].getCasillas()==4 || posicionamiento[k][i+1].getNombre().equals("DEL")){
                  if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                    System.out.print("■■■■");
                  
                    i++;
                    if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                      System.out.print("■■■■");
                      i++;
                      
                      if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                        System.out.print("■ > \u001B[0m");
                        i++;
                      }else{
                        System.out.print(" X  \u001B[0m");
                        i++;
                      }
                    }else{
                      System.out.print(" X  ");
                    
                      i++;
                      if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                        System.out.print("■ > \u001B[0m");
                        i++;
                      }else{
                        System.out.print(" X  \u001B[0m");
                        i++;
                      }
                    }
                  }
                  }else{
                    System.out.print(" X  ");
                    
                    i++;
                    if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                      System.out.print("■■■■");
                      i++;
                      
                      if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                        System.out.print("■ > \u001B[0m");
                        i++;
                      }else{
                        System.out.print(" X  \u001B[0m");
                        i++;
                      }
                    }else{
                      System.out.print(" X  ");
                    
                      i++;
                      if(posicionamiento[k][i+1].getCasillas()!=posicionamiento[k][i+1].getVida() && posicionamiento[k][i+1].getCasillas() != 0){
                        System.out.print("■ > \u001B[0m");
                        i++;
                      }else{
                        System.out.print(" X  \u001B[0m");
                        i++;
                      }
                    }
                 }
                }
            }

          }
          else if (posicionamiento[k][i].getCasillas() == 1){
              System.out.print("\u001B[46;37m│\u001B[46;30m<■>\u001B[0m");
          }
          else { 
              if (posicionamiento[k][i].getCasillas() ==2){
                
                try{
                  if(k==9){
                    if (posicionamiento[k-1][i]!=posicionamiento[k][i]){

                      if (i!=9){
                      
                        if(posicionamiento[k][i]!=posicionamiento[k][i+1]){
                          
                          if(posicionamiento[k][i]!=posicionamiento[k][i+1] && !posicionamiento[k][i].getNombre().equals("DEL") && posicionamiento[k][i].getCasillas()==posicionamiento[k][i].getVida()){
                            System.out.print("\u001B[46;37m│u001B[46;30m v \u001B[0m");
                          }else{
                            System.out.print("\u001B[46;37m│ X \u001B[0m");
                          }

                        }else{
                          System.out.print("\u001B[46;37m│\u001B[46;30m< ■");
                          
                          i++;
                          if(posicionamiento[k][i].getCasillas()==0){
                            System.out.print(" X \u001B[0m");
                          }else{
                            System.out.print("■■ >\u001B[0m");
                          }
                          
                        }
                      }else{
                        System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                        
                      }
                    }else{
                      flag=true;
                      System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                      valorI.add(i);
                    }
                  
                  }else{

                    if (posicionamiento[k][i]!=posicionamiento[k+1][i]){
                      if (posicionamiento[k][i].getCasillas() ==2){
                        
                        if (i!=9){
                          if(posicionamiento[k][i]!=posicionamiento[k][i+1] && !posicionamiento[k][i].getNombre().equals("DEL") && posicionamiento[k][i].getCasillas()==posicionamiento[k][i].getVida()){
                            
                            if(posicionamiento[k][i].getCasillas() > 0){
                              System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                            }else{
                              System.out.print("\u001B[46;37m│ X \u001B[0m");
                            }

                          }else{
                            
                            System.out.print("\u001B[46;37m│\u001B[46;30m< ■");
                          
                            i++;
                            if(posicionamiento[k][i].getCasillas()==0){
                              System.out.print(" X  \u001B[0m");
                            }else{
                              System.out.print("■■ >\u001B[0m");
                            }
                          }
                        }else{
                            System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                        }
                      }
                      
                    }else{
                      flag=true;
                      System.out.print("\u001B[46;37m│\u001B[46;30m ʌ \u001B[0m");
                      valorI.add(i);
                    }
                  }
                  
                }catch(Exception e){
                }
                
              }else if(posicionamiento[k][i].getCasillas() ==3){
                try{
                  if(k==9){
                    if (posicionamiento[k-1][i]!=posicionamiento[k][i]){
                    if(i!=9 ){  
                        if(posicionamiento[k][i]!=posicionamiento[k][i+1] && !posicionamiento[k][i].getNombre().equals("DEL") && posicionamiento[k][i].getCasillas()==posicionamiento[k][i].getVida()){
                          System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                        }else{
                          System.out.print("\u001B[46;37m│\u001B[46;30m < ");
                          if(posicionamiento[k][i+1].getCasillas()==0){
                            System.out.print(" X  \u001B[0m");
                           
                          }else{
                            System.out.print("■■■■");
                          }
                          if(posicionamiento[k][i+2].getCasillas()==0){
                            System.out.print(" X  \u001B[0m");
                          }else{
                            System.out.print("■ > \u001B[0m");
                          }
                          i+=2;
                        }
                    }else{
                      System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                    }
                  }else{
                    flag=true;
                    valorI.add(i);
                    System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                  }
                  }else{
                    if (posicionamiento[k][i]!=posicionamiento[k+1][i]){
                      if(i!=9){  
                          if(posicionamiento[k][i]!=posicionamiento[k][i+1] && !posicionamiento[k][i].getNombre().equals("DEL") && posicionamiento[k][i].getCasillas()==posicionamiento[k][i].getVida()){
                            System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                          }else{
                            System.out.print("\u001B[46;37m│\u001B[46;30m < ");
                          if(posicionamiento[k][i+1].getCasillas()==0){
                            System.out.print(" X  ");
                           
                          }else{
                            System.out.print("■■■■");
                          }
                          if(posicionamiento[k][i+2].getCasillas()==0){
                            System.out.print(" X  \u001B[0m");
                          }else{
                            System.out.print("■ > \u001B[0m");
                          }
                            i+=2;
                          }
                      }else{
                        System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                      }
                    }else{
                      flag=true;
                      valorI.add(i);
                      if(k==0){
                        System.out.print("\u001B[46;37m│\u001B[46;30m ʌ \u001B[0m");
                      }else{
                        if (posicionamiento[k-1][i]!=posicionamiento[k][i]){
                          System.out.print("\u001B[46;37m│\u001B[46;30m ʌ \u001B[0m");
                        }else{
                          System.out.print("\u001B[46;37m│\u001B[46;30m █ \u001B[0m");
                        }
                      }
                    }
                  }
                }catch(Exception e){
                  
                }
              }else{
                try{
                  if(k==9){
                    if (posicionamiento[k-1][i]!=posicionamiento[k][i]){
                    if(i!=9){  
                        if(posicionamiento[k][i]!=posicionamiento[k][i+1] && !posicionamiento[k][i].getNombre().equals("DEL") && posicionamiento[k][i].getCasillas()==posicionamiento[k][i].getVida()){
                          System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                        }else{

                          System.out.print("\u001B[46;37m│\u001B[46;30m < ");
                          if(posicionamiento[k][i+1].getCasillas()==0){
                            System.out.print(" X  ");
                          }else{
                            System.out.print("■■■■");
                          }
                          if(posicionamiento[k][i+2].getCasillas()==0){
                            System.out.print(" X  ");
                          }else{
                            System.out.print("■■■■");
                          }
                          if(posicionamiento[k][i+3].getCasillas()==0){
                            System.out.print(" X  \u001B[0m");
                          }else{
                            System.out.print("■ > \u001B[0m");
                          }
                          
                          i+=3;
                        }
                    }else{
                      System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                    }
                  }else{
                    flag=true;
                    valorI.add(i);
                    System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                  }
                  }else{
                    if (posicionamiento[k][i]!=posicionamiento[k+1][i]){
                      if(i!=9){  
                            if(posicionamiento[k][i]!=posicionamiento[k][i+1] && !posicionamiento[k][i].getNombre().equals("DEL") && posicionamiento[k][i].getCasillas()==posicionamiento[k][i].getVida()){
                              System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                            }else{
                              System.out.print("\u001B[46;37m│\u001B[46;30m < ");
                            if(posicionamiento[k][i+1].getCasillas()==0){
                              System.out.print(" X  ");
                            }else{
                              System.out.print("■■■■");
                            }
                            if(posicionamiento[k][i+2].getCasillas()==0){
                              System.out.print(" X  ");
                            }else{
                              System.out.print("■■■■");
                            }
                            if(posicionamiento[k][i+3].getCasillas()==0){
                              System.out.print(" X  \u001B[0m");
                            }else{
                              System.out.print("■ > \u001B[0m");
                            }
                            i+=3;
                          }
                      }else{
                        System.out.print("\u001B[46;37m│\u001B[46;30m v \u001B[0m");
                      }
                    }else{
                      flag=true;
                      valorI.add(i);
                      if(k==0){
                        System.out.print("\u001B[46;37m│\u001B[46;30m ʌ \u001B[0m");
                      }else{
                        if (posicionamiento[k-1][i]!=posicionamiento[k][i]){
                          System.out.print("\u001B[46;37m│\u001B[46;30m ʌ \u001B[0m");
                        }else{
                          System.out.print("\u001B[46;37m│\u001B[46;30m █ \u001B[0m");
                        }
                      }
                    }
                  }
                }catch(Exception e){
                  
                }
              }
                  
          }
      }
      impreso=false;
      System.out.print("\u001B[46;37m│\u001B[0m\n");
    }
      System.out.print("  \u001B[46;37m·---------------------------------------·\u001B[0m\n");
  }

  public void posicionarBarcos(){
    //Función encargada de solicitar al usuario las posiciones donde colocar los barcos, tiene validaciones para cada barco en específico y también para cuando se intenta colocar barcos en casillas ya ocupadas, este metodo no recibe ni retorna nada
    int posInicial;
    int posFinal;
    Barco barco;
    int temp;
    boolean casillasCorrectas;
    boolean casillasUsadas;
    for (int i = 0; i < 9; i++){
        
        barco = jugador.getFlota().get(i);
        
        if (barco.getCasillas() < 2){
            casillasUsadas = true;
            do {
              System.out.println("Ingrese la casilla donde colocará su barco Coraza (1)");
              posInicial = validarPosicion(entrada.next().toUpperCase());
              
              if (posicionamiento[posInicial / 10][posInicial % 10].getCasillas() > -1){
                System.out.println("Ingresó una casilla que ya tiene un barco");
              }
              else {

                posicionamiento[posInicial / 10][posInicial % 10] = barco;
                casillasUsadas = false;
              }
            }while (casillasUsadas == true);
        }
        else if (barco.getCasillas() < 3){
             casillasCorrectas = false;
            do{
            
                System.out.println("Ingrese la casilla inicial donde colocará su barco Destructor (2)");
                posInicial = validarPosicion(entrada.next().toUpperCase());
                System.out.println("Ingrese la casilla final donde colocará su barco Destructor (2)");
                posFinal = validarPosicion(entrada.next().toUpperCase());
                
                if (posFinal < posInicial){
                    temp = posInicial;
                    posInicial = posFinal;
                    posFinal = temp;
                }

                if ((posFinal - posInicial) != 1 && (posFinal - posInicial) != 10){
                  System.out.println("\nEl largo del barco debe ser de dos");
                    continue;
                }else{

                  if (posInicial / 10 == posFinal / 10){
                    
                    if (posicionamiento[posInicial / 10][posInicial % 10].getCasillas() > -1 || posicionamiento[posFinal / 10][posFinal % 10].getCasillas() > -1){
                      System.out.println("Ingresó una casilla que ya tiene un barco");
                      
                    }
                    else{
                      for (int j = posInicial; j <= posFinal; j++){
                        posicionamiento[j / 10][j % 10] = barco;
                      }
                      casillasCorrectas = true;
                    }
                  }
                  else {
                    if (posicionamiento[posInicial / 10][posInicial % 10].getCasillas() > -1 || posicionamiento[posFinal / 10][posFinal % 10].getCasillas() > -1){
                      System.out.println("Ingresó una casilla que ya tiene un barco");
                      
                    }
                    else{
                    for (int j = posInicial; j <= posFinal; j += 10){
                      posicionamiento[j / 10][j % 10] = barco;
                    }
                    casillasCorrectas = true; 
                  }
                }
                  
              }
            }while (casillasCorrectas == false);
        }
        else if (barco.getCasillas() < 4){
          casillasCorrectas = false;

            do{
              System.out.println("Ingrese la casilla inicial donde colocará su barco Submarino (3)");
              posInicial = validarPosicion(entrada.next().toUpperCase());            
              System.out.println("Ingrese la casilla final donde colocará su barco Submarino (3)");
              posFinal = validarPosicion(entrada.next().toUpperCase());

              if (posFinal < posInicial){
                    temp = posInicial;
                    posInicial = posFinal;
                    posFinal = temp;
                }

                if ((posFinal - posInicial) != 2 && (posFinal - posInicial) != 20){
                  System.out.println("\nEl largo del barco debe ser de tres");
                    continue;
                }else{

                  if (posInicial / 10 == posFinal / 10){

                    if (posicionamiento[posInicial / 10][posInicial % 10].getCasillas() > -1 || posicionamiento[(posInicial +1)/10][(posInicial +1)%10].getCasillas() > -1 || posicionamiento[posFinal / 10][posFinal % 10].getCasillas() > -1){
                      System.out.println("Ingresó una casilla que ya tiene un barco");
                      
                    }
                    else {
                    for (int j = posInicial; j <= posFinal; j++){
                      posicionamiento[j / 10][j % 10] = barco;
                    }
                    casillasCorrectas = true;
                  }
                  }
                  else {

                    if (posicionamiento[posInicial / 10][posInicial % 10].getCasillas() > -1 || posicionamiento[(posInicial +10)/10][(posInicial +10)%10].getCasillas() > -1 || posicionamiento[posFinal / 10][posFinal % 10].getCasillas() > -1){
                      System.out.println("Ingresó una casilla que ya tiene un barco");
                      
                    }
                    else {

                    for (int j = posInicial; j <= posFinal; j += 10){
                      posicionamiento[j / 10][j % 10] = barco;
                    }
                    casillasCorrectas = true;
                    } 
                }
              }
            }while (casillasCorrectas == false);
        }
        else{
          casillasCorrectas = false;

            do{
              System.out.println("Ingrese la casilla inicial donde colocará su barco Portaaviones (4)");
              posInicial = validarPosicion(entrada.next().toUpperCase());            
              System.out.println("Ingrese la casilla final donde colocará su barco Portaaviones (4)");
              posFinal = validarPosicion(entrada.next().toUpperCase());

              if (posFinal < posInicial){
                    temp = posInicial;
                    posInicial = posFinal;
                    posFinal = temp;
                }

                if ((posFinal - posInicial) != 3 && (posFinal - posInicial) != 30){
                  System.out.println("\nEl largo del barco debe ser de cuatro");
                    continue;
                }else{

                  if (posInicial / 10 == posFinal / 10){

                    if (posicionamiento[posInicial / 10][posInicial % 10].getCasillas() > -1 || posicionamiento[(posInicial +1)/10][(posInicial +1)%10].getCasillas() > -1 || posicionamiento[(posInicial +2)/10][(posInicial +2)%10].getCasillas() > -1 || posicionamiento[posFinal / 10][posFinal % 10].getCasillas() > -1){
                      System.out.println("Ingresó una casilla que ya tiene un barco");
                      
                    }

                    else {
                    for (int j = posInicial; j <= posFinal; j++){
                      posicionamiento[j / 10][j % 10] = barco;
                    }
                    casillasCorrectas = true;
                    }
                  }
                  else {

                    if (posicionamiento[posInicial / 10][posInicial % 10].getCasillas() > -1 || posicionamiento[(posInicial +10)/10][(posInicial +10)%10].getCasillas() > -1 || posicionamiento[(posInicial +20)/10][(posInicial +20)%10].getCasillas() > -1 || posicionamiento[posFinal / 10][posFinal % 10].getCasillas() > -1){
                      System.out.println("Ingresó una casilla que ya tiene un barco");
                      
                    }
                    else{
                    for (int j = posInicial; j <= posFinal; j += 10){
                      posicionamiento[j / 10][j % 10] = barco;
                    }
                    casillasCorrectas = true;
                    } 
                }
              }
            }while (casillasCorrectas == false);
        }
    imprimirTablero();
    }
  }

  
  public int validarPosicion(String posicion){
    //Método para validar y convertir a entero la entrada del usuario, el número retornado estará entre 0 y 99.
    boolean validacion = false;
    
    if (posicion.length() != 2){
        if (posicion.length() == 3){
            if (posicion.charAt(1) == 49 && posicion.charAt(2) == 48){
                int temp = posicion.charAt(0) - 65;
                if (temp >= 10 || temp < 0){
                    System.out.println("Ingresó una letra no valida");
                    System.out.print("Ingrese nuevamente la posición: ");
                    return validarPosicion(entrada.next());
                }
                else{
                    return (temp*10) + 9;
                }
            }
            else {
                System.out.println("Favor ingresar una posición valida");
                System.out.print("Ingrese nuevamente la posición: ");
                return validarPosicion(entrada.next());
            }   
        }
        else {
            System.out.println("Favor ingresar una posición valida");
            System.out.print("Ingrese nuevamente la posición: ");
            return validarPosicion(entrada.next());
        }
    }
    int valorX = posicion.charAt(0) - 65;
    int valorY = posicion.charAt(1) - 49;
    
    
    while (!validacion){
        
        valorX = posicion.charAt(0) - 65;
        valorY = posicion.charAt(1) - 49;
    
        
        if (valorX >= 10 || valorX < 0){
            System.out.println("Ingresó una letra no valida");
            if (valorY >= 10 || valorY < 0){
                System.out.println("Ingresó un número no valido");
            }
            System.out.println("Ingrese nuevamente la posición");
            posicion = entrada.next();
        }
        else if (valorY >= 10 || valorY < 0){
            System.out.println("Ingresó un número no valido");
            System.out.println("Ingrese nuevamente la posición");
            posicion = entrada.next();
        }
        else {validacion = true;}
    }
    
    return (valorX * 10) + valorY;
  }
  
  public void registrarMovimiento(Barco[][] tableroEnemigo, Jugador jug){
    //Función encargada de ver si el movimiento hecho por un jugador le dio a un barco o no, también realiza validaciones para ver si la casilla ya había sido seleccionada, recibe el tablero y el jugador enemigo de la persona que esta registrando el movimiento, y no retorna nada
    boolean acierto = false;

    do {

        System.out.println("Ingrese la posición a la que desea disparar");
        int posicion = validarPosicion(entrada.next());

        if (jugador.getAcierto().contains(posicion) || jugador.getMovimiento().contains(posicion)){
          System.out.println("\nIngresó una casilla que ya había ingresado\n");
          registrarMovimiento(tableroEnemigo, jug);
          acierto = false;
        }
        else{


          if(tableroEnemigo[posicion/10][posicion%10].getCasillas()>0){
              System.out.println("\nBOOOM, barco impactado\n");

              try {
                Thread.sleep(250);
              } catch (Exception e) {
              System.out.println(e);
              }

              tableroEnemigo[posicion/10][posicion%10].reducirVida();
              jugador.getAcierto().add(posicion);

              if (tableroEnemigo[posicion/10][posicion%10].getVida()==0){
                jug.getFlota().remove(tableroEnemigo[posicion/10][posicion%10]);
                System.out.println("              _.-^^---....,,---_\n           _--                  --_\n          <          BOOM!         >)\n           \\._                   _./\n              ```--. . , ; .--'''\n                    | |   |\n                 .-=||  | |=-.\n                 `-=#$%&%$#=-'\n                    | ;  :|\n           _____.,-#%&$@%#&#~,._____\n\n Barco Destruido");

              }
              try {
              Thread.sleep(1*1000);
              } catch (Exception e) {
              System.out.println(e);
              }

              

              tableroEnemigo[posicion/10][posicion%10]= new Barco("DEL",0);
              acierto = true;

              imprimirTableroMovimiento();

              if (jug.getFlota().size() == 0){
                acierto = false;
              }
              
          }else {
            jugador.getMovimiento().add(posicion);
            System.out.println("           _           _     \n          | |         | |    \n ___ _ __ | | __ _ ___| |__  \n/ __| '_ \\| |/ _` / __| '_ \\ \n\\__ \\ |_) | | (_| \\__ \\ | | |\n|___/ .__/|_|\\__,_|___/_| |_|\n    | |\n    |_|\n");
            try {
              Thread.sleep(1*1000);
            } catch (Exception e) {
              System.out.println(e);
            }
            System.out.println("Al agua");
            try {
              Thread.sleep(1*1000);
            } catch (Exception e) {
              System.out.println(e);
            }
            acierto = false;
          }
        }


    }while(acierto == true);
  }
    
  
}