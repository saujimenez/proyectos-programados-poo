/*
Progragación Orientada a Objetos. Proyecto #2 Batalla Naval
Instituto Tecnológico de Costa Rica. II Semestre 2019
Prof. Samanta Ramijan Carmiol

Estudiantes:
          Randy Conejo Juárez
          Kevin Zumbado Cruz
          Saúl Jiménez González
*/
import java.util.Scanner;

class Main {
  /*En main lo unico que posee es menu*/
  
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int opc=0;


    do{
        System.out.println();
        System.out.println("██████╗  █████╗ ████████╗ █████╗ ██╗     ██╗      █████╗     ███╗   ██╗ █████╗ ██╗   ██╗ █████╗ ██╗");
        System.out.println("██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██║     ██║     ██╔══██╗    ████╗  ██║██╔══██╗██║   ██║██╔══██╗██║");
        System.out.println("██████╔╝███████║   ██║   ███████║██║     ██║     ███████║    ██╔██╗ ██║███████║██║   ██║███████║██║");
        System.out.println("██╔══██╗██╔══██║   ██║   ██╔══██║██║     ██║     ██╔══██║    ██║╚██╗██║██╔══██║╚██╗ ██╔╝██╔══██║██║");
        System.out.println("██████╔╝██║  ██║   ██║   ██║  ██║███████╗███████╗██║  ██║    ██║ ╚████║██║  ██║ ╚████╔╝ ██║  ██║███████╗");
        System.out.println("╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═══╝╚═╝  ╚═╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝");
        System.out.println();
        System.out.println("\t.--------------------------------------------------------------------------------------------.");
        System.out.println("\t│  1. Jugar                     🚢                                                           │");
        System.out.println("\t│  2. Cómo Jugar                                       🚢                                    │");
        System.out.println("\t│  0. Salir                                                                    🚢            │");
        System.out.println("\t.--------------------------------------------------------------------------------------------.");
        opc = sc.nextInt();
        switch(opc)
        {
          case 1:
            Controlador ctr = new Controlador();
          break;

          case 2:
            System.out.println("En batalla naval hay que tratar de eliminar la flota completa del equipo enemigo.");
            System.out.println("Al iniciar, cada jugador va a colocar los barcos de su flota, cuando ambos están listos, se continúa con la destrucción.");
            System.out.println("Para posicionar un barco, se debe dar la coordenada guiándose con el tablero. Ej: A1 en mayuscula y sin espacios");
            System.out.println("Para los barcos grandes, se debe dar la posición inicial del barco, luego se da la posición final. Ej: A1, se presiona enter, luego A2, lo que crearía un destructor en esas casillas.");
            System.out.println("A cada jugador se le indica cuál barco está colocando.");
            System.out.println("Cada jugador cuenta con: Dos corazas(1), Tres destructores(2), Tres submarinos(3) y Un portaaviones(4). ");
            System.out.println("Para disparar se usa la misma estrategia que al colocar los barcos, pero sólamente se podrá elegir una casilla a la vez");
            System.out.println("Si un jugador acierta a un barco, podrá disparar de nuevo, hasta que falle");
            System.out.println("El jugador que deje sin barcos al otro, gana.");
            System.out.println("\nPresiona 1 para jugar");
            System.out.println("Presiona 0 para salir\n");

            int i = sc.nextInt();
            switch(i)
            {
              case 1:
                Controlador ctr2 = new Controlador();
              break;

              default:
                opc = 0;
              break;    

            }

          default:
            opc=0;
          break;
        }
    }while (opc!=0);
  }
}
    